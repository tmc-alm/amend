@echo off
:: Version 1.0
:: Buildscript
set CHANGEDMIDDLE=1
set CHANGEDNEWEST=1

:: For loop to get commit message
for /F "delims=" %%I in ('git log --oneline') do (
    for /f "tokens=2 " %%G IN ("%%I") DO ( 
        if "%%G" == "Udated" (
            set CHANGEDMIDDLE=0
        )
    )
)

:: For loop to get commit message about the bild
for /F "delims=" %%J in ('git log --oneline') do (
    for /f "tokens=2 " %%K IN ("%%J") DO ( 
        if "%%K" == "Bild" (
            set CHANGEDNEWEST=0
        )
    )    
)

:: Print when the change in the middle commit message was made
if %CHANGEDMIDDLE% EQU 1 (
    echo Middle commmit message changed
) else (
    echo Middle commit message not changed
)

:: Print when the change in the newest commit message was made
if "%CHANGEDNEWEST%" == "1" (
    echo Newest commmit message changed
) else (
    echo Newest commit message not changed
)

pause
